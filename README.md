# AAAStartup

This GNOME Shell Extension starts a set of applications and moves them to a pre-defined place in a 2x2 grid, supporting multiple monitors and workspaces.

![AAAStartup](./img/screenshot.png)

The left button starts all windows, the right button sends them to their designated place.

It is configured via a JSON object located in the [config.js](config.js) file.
Each app is represented by an object within the config array and has the following options:

* `command: Array[]`, *required*\
  The command with which the application is started, were you to run it
  from a command line. The array can contain multiple arguments to reflect
  command-line arguments passed in, e.g.
  `[ '/usr/bin/firefox', '-private', 'https://duckduckgo.com' ]`.
  Since 3.38.4 this apparently needs the full path for the application.
  
* `title: String`, *optional*\
  The title the started window shows, obtained by starting it and checking.
  This will match any window, which *contains* the given title, e.g. `Mozilla Firefox`.
  This matches the window's name (the first line of a window, shown in blue) in
  [LookingGlass](https://wiki.gnome.org/Projects/GnomeShell/LookingGlass)'s
  "Windows" pane.

* `app: String`, *optional*\
  The application name as per [Window.get_wm_class()](https://gjs-docs.gnome.org/meta7~7_api/meta.window#method-get_wm_class). This matches the "wmclass" in
  [LookingGlass](https://wiki.gnome.org/Projects/GnomeShell/LookingGlass)'s
  "Windows" pane, e.g. `Firefox`.

* `workspace: String`, *required*\
  The name of the workspace on which this window should be placed.
  Workspaces can be created and named using
  [dconf-editor](https://wiki.gnome.org/Apps/DconfEditor), and are found under
  `/org/gnome/desktop/wm/preferences/workspace-names`, e.g. `Workspace 1`.

* `screen: int`, *required*\
  The screen on which the window should be placed. This assumes screens are
  numbered, starting at 0 and increasing from left to right, e.g. `1`

* `position: Array`, *required*\
  The space the window should occupy. Leave empty for full size.
  Options are: `left`, `right`, `top`, `bottom` or combinations thereof,
  e.g. `['left', 'bottom']`.
  Since windows are rectangular, they can only occupy one quarter, one half or
  the entire screen, but not three quarters.
  
If something doesn't work you can debug with `journalctl /usr/bin/gnome-shell -f -o cat`.
You may have to reload the extension using `Alt+F2` followed by `r` to reload gnome shell.

Notes:
* Both title and app are optional and will not be considered if left out.
  However, if neither of them are set, no windows will be matched.
* Only one window with the same title and class is currently supported.
* The title/class workaround for selecting windows is needed as process IDs
  seem to be broken, i.e. the process ID assigned by the spawn method is not
  known by the OS.
* The grid is hardcoded, so 3x3, 2x3, and any other grids are supported.
  It would't be hard to add support but I have no intention of doing that.
* I created this extension because I wanted to use it and couldn't find
  anything like this. I have no plans to make it more convenient, e.g. using a
  settings GUI - it works for me and I don't need anything else.
  Be my guest to for it if you feel it could do with a bit more attention. 🤓
* Yes, the name is a bit stupid - I wanted to easily find it in my many extensions
  so i went for something at the start of the alphabet.

