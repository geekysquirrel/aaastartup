/*
Copyright 2020 Stefanie Wiegand

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
This sample setup assumes you operate two workspaces ('Admin' and 'Coding')
and three screens (0, 1 and 2), arranged horizontally from left to right.
*/
const config = [
    {
        command: [ '/usr/bin/firefox' ],
        title: 'Mozilla Firefox', app: 'Firefox',
        workspace: 'Coding', screen: 1, position: ['right']
    },
    {
        command: [ 'gnome-terminal',  '/path/to/my/code' ],
        title: 'fish /home/stef', app: 'Gnome-terminal',
        workspace: 'Coding', screen: 1, position: ['top', 'right']
    },
    {
        command: [ 'nemo',  '/path/to/my/code' ],
        title: 'code', app: 'Nemo',
        workspace: 'Coding', screen: 1, position: ['bottom', 'right']
    },
    {
        command: [ '/usr/share/code/code' ],
        title: 'Welcome - Visual Studio Code', app: 'Code',
        workspace: 'Coding', screen: 2, position: []
    },
    {
        command: [ '/usr/bin/firefox',  '-new-window', 'https://accounts.google.com/signin/v2/identifier?&service=mail' ],
        title: 'Gmail', app: 'Firefox',
        workspace: 'Admin', screen: 1, position: ['left']
    },
    {
        command: [ '/usr/bin/keepassxc' ],
        title: 'KeePassXC', app: 'KeePassXC',
        workspace: 'Admin', screen: 1, position: ['right']
    },
    {
        command: [ '/usr/share/code/code',  '--new-window', '/path/to/my/diary/diary.code-workspace' ],
        title: 'diary (Workspace) - Visual Studio Code', app: 'Code',
        workspace: 'Admin', screen: 2, position: ['left']
    },
    {
        command: [ '/usr/bin/slack' ],
        title: 'Slack', app: 'Slack',
        workspace: 'Admin', screen: 2, position: ['right']
    }
]
