/*
Copyright 2020 Stefanie Wiegand

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

'use strict';
// debug using `journalctl /usr/bin/gnome-shell -f -o cat`

const Gio = imports.gi.Gio;         // https://gjs-docs.gnome.org/gio20~2.66p/
const St = imports.gi.St;           // https://gjs-docs.gnome.org/st10/
const Clutter = imports.gi.Clutter;
const Main = imports.ui.main;
const Meta = imports.gi.Meta;       // https://gjs-docs.gnome.org/meta7~7_api/
const Shell = imports.gi.Shell;     // https://gjs-docs.gnome.org/shell01/
const Glib = imports.gi.GLib;       // https://gjs-docs.gnome.org/glib20~2.66.1/
const Util = imports.misc.util;
const Mainloop = imports.mainloop;

/* Import PanelMenu and PopupMenu */
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const config = Me.imports.config;

// Global variable : GLOBAL_BUTTON to click in the topbar
var GLOBAL_BUTTON;
var APPS = config.config;

var _log = function(text) {
    log(`[AAAStartupMenu]: ${text}`)
}

class AAAStartupMenu {

    constructor() {
		this.super_btn = new PanelMenu.Button(0.0, _("AAAStartupMenu"), false);
		let box = new St.BoxLayout();
		let icon = new St.Icon({
			icon_name: 'media-playback-start-symbolic',
			style_class: 'system-status-icon'
		});
		box.add_child(icon);
		
		this.super_btn.add_child(box);
		this.super_btn.visible = true;
		
		// make menu
		this._buttonMenuItem = new PopupMenu.PopupBaseMenuItem({
			reactive: false,
			can_focus: false
		});
		
		//make buttons
		this.adminButton = new St.Button({
			reactive: true,
			can_focus: true,
			track_hover: true,
			toggle_mode: false,
			accessible_name: 'Start apps',
			style_class: 'runButton',
			child: new St.Icon({
			    //icon_name: 'accessories-dictionary-symbolic', icon_size: 20
			    icon_name: 'system-run-symbolic', icon_size: 20
			}),
			x_expand: true,
			x_align: Clutter.ActorAlign.CENTER,
		});
		this.adminButton.connect('clicked', this._runApps.bind(this));
		
		this.codeButton = new St.Button({
			reactive: true,
			can_focus: true,
			track_hover: true,
			toggle_mode: false,
			accessible_name: 'Place apps',
			style_class: 'runButton',
			child: new St.Icon({
			    //icon_name: 'gnome-terminal-symbolic', icon_size: 20
				icon_name: 'document-send-symbolic', icon_size: 20
			}),
			x_expand: true,
			x_align: Clutter.ActorAlign.CENTER,
		});
		this.codeButton.connect('clicked', this._sortApps.bind(this));
		
		// add menu items
		this._buttonMenuItem.actor.add_child(this.adminButton);
		this._buttonMenuItem.actor.add_child(this.codeButton);
		
		// add menu to button
		this.super_btn.menu.addMenuItem(this._buttonMenuItem);
    }
 
	_runApps() {
		_log('Starting apps...')
		// start apps
		APPS.map(app => {
		    _log('---------------')
	        _log(`Creating new window for ${app.command}`)
	        let [success, pid] = Glib.spawn_async(
                null, // cwd
                app.command, // argv
                null, // env
                null, //Use env path and no repeat
                null // child_setup
            );
			_log(`Started app with PID ${pid}`)
		})
	}

	_sortApps() {
	    _log('Getting workspace/screen dimensions')
	    let workspaces = []
	    let screens = []
	    // get all workspaces
	    for ( let wks=0; wks<global.workspace_manager.n_workspaces; ++wks ) {
            workspaces.push(Meta.prefs_get_workspace_name(wks))
        }
        _log(`Workspaces: ${workspaces}`)
        // use first screen to get monitor dimensions
        let metaWorkspace = global.workspace_manager.get_workspace_by_index(0)
        // for some reason these are the wrong way round...
        for (let n = 0; n < metaWorkspace.get_display().get_n_monitors(); n++) {
            let r = metaWorkspace.get_work_area_for_monitor(n)
            screens.push({ x: r.x, y: r.y, w: r.width, h: r.height })
        }
        // sort by top/left x coord - that way the most left one will be considered 0
        screens.sort((a, b) => (Number(a.x) > Number(b.x) ? true : false))
        _log(`Screens: ${screens.map(s => `[${s.x},${s.y} - ${s.w}x${s.h}]`)}`)
	
	    APPS.map(app => {
			let win = this._getWindowByTitle(app.title, app.app)
			if (win !== undefined) {
			    _log('---------------')
			    _log(`Moving/resizing "${app.title}"`)
			    // move to correct workspace and unmaximise
			    win.unmaximize(Meta.MaximizeFlags.BOTH)
			    win.change_workspace_by_index(workspaces.indexOf(app.workspace), false)
			    // get current bounding rectangle
	            let r = win.get_frame_rect()
	            _log(` - Current dimensions: ${r.x} ${r.y} ${r.width} ${r.height}`)
	            // start at 0,0 of its screen by default
	            let x = screens[app.screen].x
	            let y = screens[app.screen].y
	            // start with full size by default
	            let w = screens[app.screen].w
	            let h = screens[app.screen].h
	            // resize?
			    if (app.position.includes('left') || app.position.includes('right')) {
			        w = Math.floor(w/2)
			    }
			    if (app.position.includes('top') || app.position.includes('bottom')) {
			        h = Math.floor(h/2)
			    }
	            //move?
			    if (app.position.includes('right')) {
		            x += Math.floor(screens[app.screen].w / 2)
		        }
		        if (app.position.includes('bottom')) {
		            y += Math.floor(screens[app.screen].h / 2)
		        }
			    // update window object
			    _log(` - New dimensions: ${x} ${y} ${w} ${h}`)
	            // move to the correct coordinates
	            win.move_resize_frame(true, x, y, w, h) 
            }
        })
	}
	
	// workaround because spawned process pid is apparently not a real pid
	_getWindowByTitle(title, appClass) {
	    for (let w of global.get_window_actors()) {
			let window = w.get_meta_window()
			if (!title && !appClass) { continue }
	        if (title && !window.get_title().includes(title)) {
	            continue
	        }
	        if (appClass && window.get_wm_class() != appClass) {
                continue
            }
            return window
	    }
	    _log(`No window found for title ${title}`)
	}
	
	/*_getWindowByPid(pid) {
	    for (let w of global.get_window_actors()) {
	        let window = w.get_meta_window()
	        if (window.get_pid()==pid) {
                return window
            }
	    }
	    _log(`No window found for PID ${pid}`)
	}*/
}

function init() {
    _log(`initializing ${Me.metadata.name} version ${Me.metadata.version}`);
}

function enable() {
    _log(`enabling ${Me.metadata.name} version ${Me.metadata.version}`);
    GLOBAL_BUTTON = new AAAStartupMenu();
	Main.panel.addToStatusArea('AAAStartup', GLOBAL_BUTTON.super_btn, 0, 'right');
}


function disable() {
    _log(`disabling ${Me.metadata.name} version ${Me.metadata.version}`);
    GLOBAL_BUTTON.super_btn.destroy();
}
